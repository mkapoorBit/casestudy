package com.impetus.booking.activemq;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.JmsListener;

import com.impetus.booking.model.BookingInfo;
import com.impetus.booking.service.BookingService;

@Configuration
public class BookingConsumer {

	@Autowired
	public BookingService bookingService;

	@JmsListener(destination = "mysample.queue")
	public void receiveBooking(BookingInfo bookingInfo) {
		bookingService.validateAndSave(bookingInfo);
	}

}
package com.impetus.booking.activemq;

import javax.jms.Queue;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import com.impetus.booking.model.BookingInfo;

@Component
public class BookingProducer {

	@Autowired
	private JmsTemplate jmsTemplate;

	@Autowired
	private Queue queue;

	public void send(final BookingInfo bookingInfo) {
		jmsTemplate.convertAndSend("mysample.queue", bookingInfo);
	}
}
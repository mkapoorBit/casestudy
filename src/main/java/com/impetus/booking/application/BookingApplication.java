package com.impetus.booking.application;

import java.util.Arrays;

import javax.jms.Queue;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.listener.DefaultMessageListenerContainer;
import org.springframework.jms.listener.MessageListenerContainer;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.SimpleMessageConverter;
import org.springframework.web.client.RestTemplate;

/**
 * 
 * @author meghna.kapoor
 *
 */
@SpringBootApplication
@ComponentScan({ "com.impetus.booking.repository",
		"com.impetus.booking.service", "com.impetus.booking.activemq",
		"com.impetus.booking.controller" })
@EnableJms
@Configuration
public class BookingApplication {

	public static void main(String args[]) {
		SpringApplication.run(BookingApplication.class, args);
	}

	@Bean
	RestTemplate restTemplate() {
		return new RestTemplate();
	}

	@Bean
	public Queue queue() {
		return new ActiveMQQueue("mysample.queue");
	}

	@Bean
	public ActiveMQConnectionFactory activeMQConnectionFactory() {
		ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory(
				"tcp://localhost:61616");
		factory.setTrustedPackages(Arrays.asList("com.impetus.booking.model",
				"java.sql.Date"));
		return factory;
	}

	@Bean
	public MessageListenerContainer getContainer() {
		DefaultMessageListenerContainer container = new DefaultMessageListenerContainer();
		container.setConnectionFactory(activeMQConnectionFactory());
		container.setDestinationName("mysample.queue");
		container.setAutoStartup(false);
		return container;
	}

	@Bean
	public JmsTemplate jmsTemplate() {
		JmsTemplate template = new JmsTemplate();
		template.setConnectionFactory(activeMQConnectionFactory());
		template.setDefaultDestinationName("mysample.queue");
		return template;
	}

	@Bean
	MessageConverter converter() {
		return new SimpleMessageConverter();
	}
}

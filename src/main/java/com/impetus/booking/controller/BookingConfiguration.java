package com.impetus.booking.controller;

import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.sql.DataSource;

import org.springframework.boot.orm.jpa.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;

/**
 * 
 * @author meghna.kapoor
 *
 */
@Configuration
@ComponentScan({ "com.impetus.booking.repository",
		"com.impetus.booking.service", "com.impetus.booking.controller" })
@EntityScan("com.impetus.booking.entity")
@EnableJpaRepositories("com.impetus.booking.entity")
@PropertySource("classpath:db-config.properties")
public class BookingConfiguration {

	protected Logger logger;

	public BookingConfiguration() {
		logger = Logger.getLogger(getClass().getName());
	}

	@Bean
	public DataSource dataSource() {
		logger.info("dataSource() invoked");

		// Create an in-memory H2 relational database containing some demo
		// accounts.
		DataSource dataSource = (new EmbeddedDatabaseBuilder())
				.addScript("classpath:testdb/schema.sql")
				.addScript("classpath:testdb/data.sql").build();

		logger.info("dataSource = " + dataSource);

		// Sanity check
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<Map<String, Object>> rooms = jdbcTemplate
				.queryForList("SELECT * FROM BOOK_ROOM");
		logger.info("System has " + rooms.size() + " rooms");

		return dataSource;
	}
}

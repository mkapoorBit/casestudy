package com.impetus.booking.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.impetus.booking.model.BookingInfo;
import com.impetus.booking.model.BookingResponse;
import com.impetus.booking.service.BookingService;

/**
 * 
 * @author meghna.kapoor
 *
 */
@RestController
@RequestMapping("/booking")
public class BookingController {

	@Autowired
	BookingService bookingService;

	@RequestMapping("/")
	public @ResponseBody List<BookingInfo> getAll() {
		return bookingService.getAllBookings();
	}

	@RequestMapping("/room")
	public @ResponseBody List<BookingInfo> getBookingDetailsOfRoom(
			@RequestBody BookingInfo bookingInfo) {
		return bookingService.getBookings(bookingInfo.getRoomXref());
	}

	@RequestMapping(value = "/book", method = RequestMethod.POST)
	public @ResponseBody BookingResponse save(
			@RequestBody BookingInfo bookingInfo) {
		return bookingService.save(bookingInfo);
	}

	@RequestMapping(value = "/cancel", method = RequestMethod.POST)
	public @ResponseBody BookingResponse cancel(
			@RequestBody BookingInfo bookingInfo) {
		return bookingService.cancel(bookingInfo);
	}
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public @ResponseBody BookingResponse update(
			@RequestBody BookingInfo bookingInfo) {
		return bookingService.update(bookingInfo);
	}
}

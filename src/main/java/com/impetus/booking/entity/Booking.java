package com.impetus.booking.entity;

import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * 
 * @author meghna.kapoor
 *
 */
@Entity
@Table(name = "BOOK_ROOM")
public class Booking {

	@Id
	@Column(name = "BOOKING_ID")
	private Integer id;

	@Column(name = "ROOM_XREF")
	private String roomXref;

	@Column(name = "BOOKING_DATE")
	private Date bookingDate;

	@Column(name = "BOOKING_START_TIME")
	private Timestamp startDate;

	@Column(name = "BOOKING_END_TIME")
	private Timestamp endTime;

	@Column(name = "USER_XREF")
	private String userXref;

	/**
	 * @return the userXref
	 */
	public String getUserXref() {
		return userXref;
	}

	/**
	 * @param userXref
	 *            the userXref to set
	 */
	public void setUserXref(String userXref) {
		this.userXref = userXref;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the roomXref
	 */
	public String getRoomXref() {
		return roomXref;
	}

	/**
	 * @param roomXref
	 *            the roomXref to set
	 */
	public void setRoomXref(String roomXref) {
		this.roomXref = roomXref;
	}

	/**
	 * @return the bookingDate
	 */
	public Date getBookingDate() {
		return bookingDate;
	}

	/**
	 * @param bookingDate
	 *            the bookingDate to set
	 */
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 * @return the startDate
	 */
	public Timestamp getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate
	 *            the startDate to set
	 */
	public void setStartDate(Timestamp startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the endTime
	 */
	public Timestamp getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(Timestamp endTime) {
		this.endTime = endTime;
	}

}

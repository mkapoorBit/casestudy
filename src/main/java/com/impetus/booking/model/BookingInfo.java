package com.impetus.booking.model;

import java.io.Serializable;

/**
 * 
 * @author meghna.kapoor
 *
 */
public class BookingInfo implements Serializable {

	private String roomXref;

	private String bookingDate;

	private String startTime;

	private String endTime;

	private String userXref;

	/**
	 * @return the userXref
	 */
	public String getUserXref() {
		return userXref;
	}

	/**
	 * @param userXref
	 *            the userXref to set
	 */
	public void setUserXref(String userXref) {
		this.userXref = userXref;
	}

	/**
	 * @return the roomXref
	 */
	public String getRoomXref() {
		return roomXref;
	}

	/**
	 * @param roomXref
	 *            the roomXref to set
	 */
	public void setRoomXref(String roomXref) {
		this.roomXref = roomXref;
	}

	/**
	 * @return the bookingDate
	 */
	public String getBookingDate() {
		return bookingDate;
	}

	/**
	 * @param bookingDate
	 *            the bookingDate to set
	 */
	public void setBookingDate(String bookingDate) {
		this.bookingDate = bookingDate;
	}

	/**
	 * @return the startTime
	 */
	public String getStartTime() {
		return startTime;
	}

	/**
	 * @param startTime
	 *            the startTime to set
	 */
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	/**
	 * @return the endTime
	 */
	public String getEndTime() {
		return endTime;
	}

	/**
	 * @param endTime
	 *            the endTime to set
	 */
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

}

package com.impetus.booking.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author meghna.kapoor
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GenericUserResponseDTO implements Serializable {

	private UserResponse userResponse;

	private UserInfo userInfo;

	private String message;

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message
	 *            the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

	public GenericUserResponseDTO() {

	}

	public GenericUserResponseDTO(UserResponse userResponse, UserInfo userInfo,
			String message) {
		this.userInfo = userInfo;
		this.userResponse = userResponse;
		this.message = message;
	}

	/**
	 * @return the userResponse
	 */
	public UserResponse getUserResponse() {
		return userResponse;
	}

	/**
	 * @param userResponse
	 *            the userResponse to set
	 */
	public void setUserResponse(UserResponse userResponse) {
		this.userResponse = userResponse;
	}

	/**
	 * @return the userInfo
	 */
	public UserInfo getUserInfo() {
		return userInfo;
	}

	/**
	 * @param userInfo
	 *            the userInfo to set
	 */
	public void setUserInfo(UserInfo userInfo) {
		this.userInfo = userInfo;
	}

}

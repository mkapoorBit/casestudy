package com.impetus.booking.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 
 * @author meghna.kapoor
 *
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomInfo {

	private String roomId;

	private String roomName;
	private String roomXref;

	/**
	 * @return the roomId
	 */
	public String getRoomId() {
		return roomId;
	}

	/**
	 * @param roomId
	 *            the roomId to set
	 */
	public void setRoomId(String roomId) {
		this.roomId = roomId;
	}

	/**
	 * @return the roomName
	 */
	public String getRoomName() {
		return roomName;
	}

	/**
	 * @param roomName
	 *            the roomName to set
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}

	/**
	 * @return the roomXref
	 */
	public String getRoomXref() {
		return roomXref;
	}

	/**
	 * @param roomXref
	 *            the roomXref to set
	 */
	public void setRoomXref(String roomXref) {
		this.roomXref = roomXref;
	}

}

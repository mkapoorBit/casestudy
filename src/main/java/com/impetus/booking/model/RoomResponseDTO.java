package com.impetus.booking.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class RoomResponseDTO implements Serializable {

	private RoomInfo roomInfo;

	private String roomResponseCode;

	private String roomResponseMessage;

	public RoomResponseDTO() {
		// TODO Auto-generated constructor stub
	}

	public RoomResponseDTO(RoomInfo roomInfo, String roomResponseCode,
			String roomResponseMessage) {
		this.roomInfo = roomInfo;
		this.roomResponseCode = roomResponseCode;
		this.roomResponseMessage = roomResponseMessage;
	}

	/**
	 * @return the roomInfo
	 */
	public RoomInfo getRoomInfo() {
		return roomInfo;
	}

	/**
	 * @param roomInfo
	 *            the roomInfo to set
	 */
	public void setRoomInfo(RoomInfo roomInfo) {
		this.roomInfo = roomInfo;
	}

	/**
	 * @return the roomResponseCode
	 */
	public String getRoomResponseCode() {
		return roomResponseCode;
	}

	/**
	 * @param roomResponseCode
	 *            the roomResponseCode to set
	 */
	public void setRoomResponseCode(String roomResponseCode) {
		this.roomResponseCode = roomResponseCode;
	}

	/**
	 * @return the roomResponseMessage
	 */
	public String getRoomResponseMessage() {
		return roomResponseMessage;
	}

	/**
	 * @param roomResponseMessage
	 *            the roomResponseMessage to set
	 */
	public void setRoomResponseMessage(String roomResponseMessage) {
		this.roomResponseMessage = roomResponseMessage;
	}

}

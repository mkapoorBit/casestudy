package com.impetus.booking.model;

public class UserDTO {

	private String userXref;

	/**
	 * @return the userXref
	 */
	public String getUserXref() {
		return userXref;
	}

	/**
	 * @param userXref
	 *            the userXref to set
	 */
	public void setUserXref(String userXref) {
		this.userXref = userXref;
	}

}

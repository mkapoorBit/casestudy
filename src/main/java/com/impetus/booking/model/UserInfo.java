package com.impetus.booking.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserInfo implements Serializable {

	private String userXref;

	/**
	 * @return the userXref
	 */
	public String getUserXref() {
		return userXref;
	}

	/**
	 * @param userXref
	 *            the userXref to set
	 */
	public void setUserXref(String userXref) {
		this.userXref = userXref;
	}

}

package com.impetus.booking.repository;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.impetus.booking.model.BookingInfo;

/**
 * 
 * @author meghna.kapoor
 *
 */
@Repository
public class BookingRepository {

	JdbcTemplate jdbcTemplate;

	@Autowired
	public BookingRepository(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	public List<Map<String, Object>> findAll() {

		List<Map<String, Object>> user = jdbcTemplate
				.queryForList("SELECT * FROM BOOK_ROOM");
		return user;
	}

	public List<Map<String, Object>> findBookingsForRoom(String roomXref) {

		String sql = "select * from BOOK_ROOM where ROOM_XREF =?";
		List<Map<String, Object>> listOfBookings = jdbcTemplate.queryForList(
				sql, new Object[] { roomXref });
		return listOfBookings;
	}

	public boolean findBookingsForRoom(String roomXref, String bookingDate,
			String startTime, String endTime) throws ParseException {
		SimpleDateFormat fm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date startTimeDt = fm.parse(startTime);
		Date endTimeDt = fm.parse(endTime);
		String sql = "select * from BOOK_ROOM where ROOM_XREF =? AND BOOKING_DATE=? ";
		List<Map<String, Object>> listOfBookings = jdbcTemplate.queryForList(
				sql, new Object[] { roomXref, bookingDate });
		List<BookingInfo> bookingInfoList = new ArrayList<BookingInfo>();
		for (Map map : listOfBookings) {
			BookingInfo bookingInfo = new BookingInfo();
			bookingInfo.setRoomXref(map.get("ROOM_XREF").toString());
			bookingInfo.setBookingDate(map.get("BOOKING_DATE").toString());
			bookingInfo.setStartTime(map.get("BOOKING_START_TIME").toString());
			bookingInfo.setEndTime(map.get("BOOKING_END_TIME").toString());
			bookingInfo.setUserXref(map.get("USER_XREF").toString());
			if (startTimeDt.equals(fm.parse(map.get("BOOKING_START_TIME")
					.toString()))
					&& endTimeDt.equals(fm.parse(map.get("BOOKING_END_TIME")
							.toString()))) {
				bookingInfoList.add(bookingInfo);
			}
			if (startTimeDt.after(fm.parse(map.get("BOOKING_START_TIME")
					.toString()))
					&& startTimeDt.before(fm.parse(map.get("BOOKING_END_TIME")
							.toString()))) {
				bookingInfoList.add(bookingInfo);
			}
			if (endTimeDt.after(fm.parse(map.get("BOOKING_START_TIME")
					.toString()))
					&& endTimeDt.before(fm.parse(map.get("BOOKING_END_TIME")
							.toString()))) {
				bookingInfoList.add(bookingInfo);
			}

		}
		return bookingInfoList.size() == 0;
	}

	public boolean addBookings(BookingInfo bookingInfo) {

		String sql = "INSERT INTO  BOOK_ROOM(ROOM_XREF,USER_XREF,BOOKING_START_TIME,BOOKING_DATE,BOOKING_END_TIME) VALUES (?, ?, ?,?,?)";
		int i = jdbcTemplate
				.update(sql,
						new Object[] { bookingInfo.getRoomXref(),
								bookingInfo.getUserXref(),
								bookingInfo.getStartTime(),
								bookingInfo.getBookingDate(),
								bookingInfo.getEndTime() });
		if (i > 0) {
			return true;
		} else
			return false;
	}

	public boolean delete(BookingInfo bookingInfo) {
		String sql = "delete from BOOK_ROOM where ROOM_XREF =? and USER_XREF=? and BOOKING_START_TIME=? and BOOKING_DATE=? and BOOKING_END_TIME=?";
		int i = jdbcTemplate
				.update(sql,
						new Object[] { bookingInfo.getRoomXref(),
								bookingInfo.getUserXref(),
								bookingInfo.getStartTime(),
								bookingInfo.getBookingDate(),
								bookingInfo.getEndTime() });
		if (i > 0) {
			return true;
		} else
			return false;

	}
}

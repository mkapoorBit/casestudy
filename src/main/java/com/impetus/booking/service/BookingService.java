package com.impetus.booking.service;

import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.impetus.booking.activemq.BookingConsumer;
import com.impetus.booking.activemq.BookingProducer;
import com.impetus.booking.model.BookingInfo;
import com.impetus.booking.model.BookingResponse;
import com.impetus.booking.model.GenericUserResponseDTO;
import com.impetus.booking.model.RoomResponseDTO;
import com.impetus.booking.repository.BookingRepository;

@Service
public class BookingService {

	@Autowired
	BookingRepository bookingRepository;

	RestTemplate restTemplate;

	@Autowired
	BookingService(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Autowired
	private BookingProducer bookingProducer;
	
	@Autowired
	private BookingUpdateProducer bookingUpdateProducer;

	@Autowired
	private BookingConsumer bookingConsumer;

	public List<BookingInfo> getAllBookings() {
		List<Map<String, Object>> bookingList = bookingRepository.findAll();
		List<BookingInfo> bookingInfoList = new ArrayList<BookingInfo>();
		for (Map<String, Object> map : bookingList) {
			BookingInfo bookingInfo = new BookingInfo();
			bookingInfo.setRoomXref(map.get("ROOM_XREF").toString());
			bookingInfo.setBookingDate(map.get("BOOKING_DATE").toString());
			bookingInfo.setStartTime(map.get("BOOKING_START_TIME").toString());
			bookingInfo.setEndTime(map.get("BOOKING_END_TIME").toString());
			bookingInfo.setUserXref(map.get("USER_XREF").toString());
			bookingInfoList.add(bookingInfo);
		}
		return bookingInfoList;
	}

	public List<BookingInfo> getBookings(String roomXref) {
		List<Map<String, Object>> bookingList = bookingRepository
				.findBookingsForRoom(roomXref);
		List<BookingInfo> bookingInfoList = new ArrayList<BookingInfo>();
		for (Map<String, Object> map : bookingList) {
			BookingInfo bookingInfo = new BookingInfo();
			bookingInfo.setRoomXref(map.get("ROOM_XREF").toString());
			bookingInfo.setBookingDate(map.get("BOOKING_DATE").toString());
			bookingInfo.setStartTime(map.get("BOOKING_START_TIME").toString());
			bookingInfo.setEndTime(map.get("BOOKING_END_TIME").toString());
			bookingInfo.setUserXref(map.get("USER_XREF").toString());
			bookingInfoList.add(bookingInfo);
		}
		return bookingInfoList;
	}

	public BookingResponse save(BookingInfo bookingInfo) {
		// Validate User
		GenericUserResponseDTO userResponse = validateUser(bookingInfo);
		if (userResponse.getUserInfo() == null) {
			return new BookingResponse(userResponse.getUserResponse()
					.getStatusCode(), userResponse.getUserResponse()
					.getStatusMessage()
					+ "Booking for Room "
					+ bookingInfo.getRoomXref()
					+ "for Date"
					+ bookingInfo.getBookingDate()
					+ " from "
					+ bookingInfo.getStartTime()
					+ " - "
					+ bookingInfo.getEndTime());
		}
		// Validate Room
		RoomResponseDTO roomResponse = validateRoom(bookingInfo);
		if (roomResponse.getRoomInfo() == null) {
			return new BookingResponse(roomResponse.getRoomResponseCode(),
					roomResponse.getRoomResponseMessage() + "Booking for Room"
							+ bookingInfo.getRoomXref() + "for Date"
							+ bookingInfo.getBookingDate() + " from "
							+ bookingInfo.getStartTime() + " - "
							+ bookingInfo.getEndTime());
		}

		// Validate Current and Future Dates
		try {
			boolean isNotValidDate = isNotValidDate(bookingInfo
					.getBookingDate());
			if (isNotValidDate) {
				return new BookingResponse("200",
						"Date Cannot be Past Date No Booking Allowed for "
								+ bookingInfo.getRoomXref() + "for Date"
								+ bookingInfo.getBookingDate() + " from "
								+ bookingInfo.getStartTime() + " - "
								+ bookingInfo.getEndTime());
			}
		} catch (ParseException e) {
			e.printStackTrace();
		}
		bookingProducer.send(bookingInfo);
		return new BookingResponse("100", "Booking Added to Queue for Room "
				+ bookingInfo.getRoomXref() + "for Date"
				+ bookingInfo.getBookingDate() + " from "
				+ bookingInfo.getStartTime() + " - " + bookingInfo.getEndTime());
	}

	public void validateAndSave(BookingInfo bookingInfo) {
		// Validate Availability

		boolean available;
		try {
			available = bookingRepository.findBookingsForRoom(
					bookingInfo.getRoomXref(), bookingInfo.getBookingDate(),
					bookingInfo.getStartTime(), bookingInfo.getEndTime());
			if (available) {
				System.out.println("You can do booking");
				// Save Booking
				boolean booked = bookingRepository.addBookings(bookingInfo);
				if (booked) {
					System.out
							.println("Request is submitted and status will be notified on email.");
				}
			} else {
				System.out.println("You can not do booking");
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public boolean isNotValidDate(String pDateString) throws ParseException {
		final SimpleDateFormat sf = new SimpleDateFormat("yyyy-MM-dd");
		Date date = sf.parse(pDateString);
		return date.before(sf.parse(sf.format(new Date())));
	}

	/**
	 * @param bookingInfo
	 * @return
	 */
	private RoomResponseDTO validateRoom(BookingInfo bookingInfo) {
		String roomUri = new String("http://" + "localhost"
				+ ":9080/room/{roomXref}");
		return restTemplate.getForObject(roomUri, RoomResponseDTO.class,
				URLEncoder.encode(bookingInfo.getRoomXref()));
	}

	/**
	 * @param bookingInfo
	 * @return
	 */
	private GenericUserResponseDTO validateUser(BookingInfo bookingInfo) {
		String uri = new String("http://" + "localhost"
				+ ":1080/user/validate/{userXref}");
		return restTemplate.getForObject(uri, GenericUserResponseDTO.class,
				URLEncoder.encode(bookingInfo.getUserXref()));
	}

	public BookingResponse cancel(BookingInfo bookingInfo) {
		if (bookingRepository.delete(bookingInfo)) {
			return new BookingResponse("100", "Booking Cancelled for Room "
					+ bookingInfo.getRoomXref() + "for Date"
					+ bookingInfo.getBookingDate() + " from "
					+ bookingInfo.getStartTime() + " - "
					+ bookingInfo.getEndTime());
		} else {
			return new BookingResponse("200", "Booking Not Cancelled for Room "
					+ bookingInfo.getRoomXref() + "for Date"
					+ bookingInfo.getBookingDate() + " from "
					+ bookingInfo.getStartTime() + " - "
					+ bookingInfo.getEndTime());
		}
	}

	public BookingResponse update(BookingInfo bookingInfo) {
		bookingUpdateProducer.send(bookingInfo);
		return new BookingResponse("100", "Booking Updated for Room "
				+ bookingInfo.getRoomXref() + "for Date"
				+ bookingInfo.getBookingDate() + " from "
				+ bookingInfo.getStartTime() + " - "
				+ bookingInfo.getEndTime());
	}

}
